<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

// Custom read more link text
if (!function_exists('mh_edition_lite_default_options')) {
	function mh_edition_lite_default_options() {
		$default_options = array(
			'excerpt_length' => 25,
			'excerpt_more' => esc_html__('Lire la suite', 'mh-edition-lite'),
			'sidebar' => 'right',
			'full_bg' => ''
		);
		return $default_options;
	}
}

/***** Post / Image Navigation *****/

if (!function_exists('mh_edition_lite_postnav')) {
	function mh_edition_lite_postnav() {
		global $post;
		$parent_post = get_post($post->post_parent);
		$attachment = is_attachment();
		$previous = ($attachment) ? $parent_post : get_adjacent_post(false, '', true);
		$next = get_adjacent_post(false, '', false);

		if (!$next && !$previous)
		return;

		if ($attachment) {
			$attachments = get_children(array('post_type' => 'attachment', 'post_mime_type' => 'image', 'post_parent' => $parent_post->ID));
			$count = count($attachments);
		}
		echo '<nav class="mh-post-nav-wrap clearfix" role="navigation">' . "\n";
			if ($previous || $attachment) {
				echo '<div class="mh-post-nav-prev mh-post-nav">' . "\n";
					if ($attachment) {
						if (wp_attachment_is_image($post->id)) {
							if ($count == 1) {
								echo '<a href="' . esc_url(get_permalink($parent_post)) . '">' . __('Back to article', 'mh-edition-lite') . '</a>';
							} else {
								previous_image_link('%link', __('Previous image', 'mh-edition-lite'));
							}
						} else {
							echo '<a href="' . esc_url(get_permalink($parent_post)) . '">' . __('Back to article', 'mh-edition-lite') . '</a>';
						}
					} else {
						previous_post_link('%link', __('Article précédent', 'mh-edition-lite'));
					}
				echo '</div>' . "\n";
			}
			if ($next || $attachment) {
				echo '<div class="mh-post-nav-next mh-post-nav">' . "\n";
					if ($attachment && wp_attachment_is_image($post->id)) {
						next_image_link('%link', __('Next image', 'mh-edition-lite'));
					} else {
						next_post_link('%link', __('Arcticle suivant', 'mh-edition-lite'));
					}
				echo '</div>' . "\n";
			}
		echo '</nav>' . "\n";
	}
}
add_action('mh_after_post_content', 'mh_edition_lite_postnav');

/***** Custom Editor Css *****/

function my_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );

?>