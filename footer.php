<footer class="mh-copyright-wrap">
	<div class="mh-container mh-container-inner clearfix">
		<p class="mh-copyright"><?php printf(__('Rodinux %1$s | Theme enfant de mh-edition-lite réalisé par mes soins %2$s'), date("Y"), '<a href="' . esc_url('http://www.lamastreassociationrad.fr/mentions-legales/') . '" rel="nofollow">Mentions Légales</a>'); ?></p>
	</div>
</footer>
</div><!-- .mh-container-outer -->
<?php wp_footer(); ?>
</body>
</html>